# Update Docker images over air-gap
These docs for how to operate the demonstration. 

This demo consists of the following services:
- Nginx

## Prerequisites

If you are in a closed network, these should already be installed. Refer to the platform-developer-docs for details.
- docker.io
- docker-compose
- pwgen
- dos2unix
- curl

## Quickstart
Install Pre-requisites
```
./platformplatform.sh install_prereqs
```

Generate all passwords and config and loads them into the environment variables.
If you want to re-generate passwords, delete `.pwgen` before running to create new credentials.
```
. ./platformplatform.sh pwgen
```

Add/Remove all hosts file entries
```
. ./platformplatform.sh removehost
. ./platformplatform.sh addhost 127.0.0.1 # replace IP with IP of server
```

Start everything
```
./platformplatform.sh up
```

Check status and test endpoints
```
./platformplatform.sh ps
./platformplatform.sh test
```

Stop everything
```
./platformplatform.sh down
```

Restart the platformplatform
```
./platformplatform.sh restart
```

## Other Commands
Pull everything. This is useful for pre-pulling images for minimum downtime
```
./platformplatform.sh pull
```

docker kill everything
```
./platformplatform.sh kill
```

## Dangerous Commands
*DANGER* Redeploy everything. Down everyting, DELETES ALL DATA but doesn't re-pull images
```
./platformplatform.sh redeploy
```

*DANGER* Nuke everything. DELETES ALL DATA, CONTAINERS, IMAGES, EVERYTHING! SCORCHED EARTH! You get it
```
./platformplatform.sh nuke
```
