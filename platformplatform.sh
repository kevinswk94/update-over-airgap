#!/bin/bash

# Check if sourced
(return 0 2>/dev/null) && sourced=1 || sourced=0

services=( nginx minio )
reverse_services=("${services[@]}") 

# reverse services when down-ing services
min=0
max=$(( ${#reverse_services[@]} -1 ))

while [[ min -lt max ]]
do
    # Swap current first and last elements
    x="${reverse_services[$min]}"
    reverse_services[$min]="${reverse_services[$max]}"
    reverse_services[$max]="$x"

    # Move closer
    (( min++, max-- ))
done

# remove specified host from /etc/hosts
removehost() {
    if [[ "$1" ]]
    then
        HOSTNAME=$1

        if [ -n "$(grep $HOSTNAME /etc/hosts)" ]
        then
            echo "$HOSTNAME Found in your /etc/hosts, Removing now...";
            sudo sed -i".bak" "/$HOSTNAME/d" /etc/hosts
        else
            echo "$HOSTNAME was not found in your /etc/hosts";
        fi
    else
        echo "Error: missing required parameters."
        echo "Usage: "
        echo "  removehost domain"
    fi
}

#add new ip host pair to /etc/hosts
addhost() {
    if [[ "$1" && "$2" ]]
    then
        IP=$1
        HOSTNAME=$2

        if [ -n "$(grep $HOSTNAME /etc/hosts)" ]
            then
                echo "$HOSTNAME already exists:";
                echo $(grep $HOSTNAME /etc/hosts);
            else
                echo "Adding $HOSTNAME to your /etc/hosts";
                printf "%s\t%s\n" "$IP" "$HOSTNAME" | sudo tee -a /etc/hosts > /dev/null;

                if [ -n "$(grep $HOSTNAME /etc/hosts)" ]
                    then
                        echo "$HOSTNAME was added succesfully:";
                        echo $(grep $HOSTNAME /etc/hosts);
                    else
                        echo "Failed to Add $HOSTNAME, Try again!";
                fi
        fi
    else
        echo "Error: missing required parameters."
        echo "Usage: "
        echo "  addhost ip domain"
    fi
}

list_project_images () {
    find ./pp-* -name 'docker-compose.yml' | xargs -I {} -- sh -c 'dos2unix {} && grep -v "^#" {}'  | grep image: | awk '{print $2}' | tr -d "'" | uniq | cat -v
}

test_curl () {
    echo "HTTP Status Codes:"
    curl -sL -o /dev/null -w "landing - %{http_code}\n" http://platform.net/
    curl -sL -o /dev/null -w "apt-mirror - %{http_code}\n" http://apt-mirror.platform.net/
    curl -sL -o /dev/null -w "pypi-mirror - %{http_code}\n" http://pypi-mirror.platform.net/
    curl -sL -o /dev/null -w "npm-mirror - %{http_code}\n" http://npm-mirror.platform.net/
    curl -sL  -o /dev/null -w "minio - %{http_code}\n" http://minio.platform.net/minio/login
    curl -sL -o /dev/null -w "airflow - %{http_code}\n" http://airflow.platform.net/
    curl -sL -o /dev/null -w "gitlab - %{http_code}\n" http://gitlab.platform.net/
    curl -sL -o /dev/null -w "harbor - %{http_code}\n" http://harbor.platform.net/
}

wait_for_container_state () {
    until [ `docker ps --filter "name=$1" --filter "health=$2" --format "{{.Names}}"` ]
    do
        docker ps --filter "name=$1" --format "{{.Names}} {{.Status}}"
        sleep 5
    done
    docker ps --filter "name=$1" --format "{{.Names}} {{.Status}}"
}

docker_compose_up () {
    echo -e "GET http://google.com HTTP/1.0\n\n" | nc google.com 80 > /dev/null 2>&1
    if [ $? -eq 0 ]; then
        echo "Contacted http://google.com, we seem to be Online"
    else
        echo "Cannot contact http://google.com, we seem to be Offline"
    fi
    for i in "${services[@]}"
    do
        :
        docker-compose -f ./pp-$i/docker-compose.yml up -d
    done
}
docker_compose_down () {
    for i in "${reverse_services[@]}"
    do
        :
        docker-compose -f ./pp-$i/docker-compose.yml down --remove-orphans
    done
}


case "$1" in
        addhost)
            addhost $2 platform.net
            for i in "${services[@]}"
            do
                :
                addhost $2 $i.platform.net
            done
            cat /etc/hosts
            ;;
        removehost)
            for i in "${services[@]}"
            do
                :
                removehost $i.platform.net
            done
            removehost platform.net
            cat /etc/hosts
            ;;
        install_prereqs)
            sudo apt update && sudo apt install -y dos2unix pwgen docker.io curl npm cifs-utils nfs-common unzip
            
            export docker_compose_version=1.25.4

            echo "Installing 'docker-compose' v${docker_compose_version}" \
            &&   sudo wget -cO /usr/local/bin/docker-compose https://github.com/docker/compose/releases/download/${docker_compose_version}/docker-compose-$(uname -s)-$(uname -m) \
            &&   sudo chmod 0755 /usr/local/bin/docker-compose


            # Everything working?
            docker-compose --version

            echo ""
            echo "Adding $USER to docker group."
            sudo usermod -aG docker $USER
            echo "Please re-login to refresh groups."
            ;;
            # https://docs.gitlab.com/ee/administration/troubleshooting/gitlab_rails_cheat_sheet.html
        backup)
            # GitLab
            dt=$(date +'%s_%Y_%m_%d');
            mkdir -p ${PWD}/pp-gitlab/backup/secrets/$dt
            echo "copying gitlab secrets, config is in .env"
            docker cp pp-gitlab_gitlab_1:/etc/gitlab/ ${PWD}/pp-gitlab/backup/secrets/$dt
            docker exec -t pp-gitlab_gitlab_1 gitlab-backup create
            ;;
        backup_ls)
            docker exec -it pp-gitlab_gitlab_1 ls /var/opt/gitlab/backups/
            ;;
        restore)
            echo "Copying last 7 days backups from object store..."
            mc config host add minio_host http://minio.platform.net $MINIO_PASSWORD_1 $MINIO_PASSWORD_2 --api S3v4
            mc mirror --newer-than 7d minio_host/gitlab-backups/ ${PWD}/tmp/backups/
            docker cp  ${PWD}/tmp/backups/ pp-gitlab_gitlab_1:/var/opt/gitlab/
            rm -f ${PWD}/tmp/backups/*
            
            echo "Listing available backups:"
            echo "=========================="
            docker exec -it pp-gitlab_gitlab_1 ls /var/opt/gitlab/backups | grep _gitlab_backup.tar | sed s/_gitlab_backup.tar//g
            echo "=========================="
            echo ""
            read -p 'Backup to restore: ' backup_to_restore
            docker exec -it pp-gitlab_gitlab_1 gitlab-backup restore BACKUP=$backup_to_restore
            
            echo "Listing available secrets:"
            echo "=========================="
            ls -1 ${PWD}/gitlab/backup/secrets
            echo "=========================="
            echo ""
            read -p 'gitlab-secrets.json to restore: ' secret_to_restore
            docker exec -it pp-gitlab_gitlab_1 rm -f /etc/gitlab/gitlab-secrets.json
            docker cp ${PWD}/gitlab/backup/secrets/$secret_to_restore/gitlab/gitlab-secrets.json pp-gitlab_gitlab_1:/etc/gitlab/gitlab-secrets.json
            docker exec -it pp-gitlab_gitlab_1 ls -lRa /etc/gitlab/
            docker exec -it pp-gitlab_gitlab_1 cat /etc/gitlab/gitlab-secrets.json | grep secret_token
            docker-compose -f ./gitlab/docker-compose.yml restart
            ;;
        up)
            docker_compose_up "$2"
            ;;
        logs)
            docker-compose -f ./pp-$2/docker-compose.yml logs -f
            ;;
        down)
            docker_compose_down
            ;;
        restart)
            echo "Stopping Services"
            docker_compose_down
            echo "Starting Services"
            docker_compose_up "$2"
            ;;
        redeploy)
            for i in "${reverse_services[@]}"
            do
                :
                docker-compose -f ./pp-$i/docker-compose.yml down -v --remove-orphans
            done
            docker_compose_up "$2"
            ;;        
        ps)
            echo -e "GET http://google.com HTTP/1.0\n\n" | nc google.com 80 > /dev/null 2>&1
            if [ $? -eq 0 ]; then
                echo "Contacted http://google.com, we seem to be Online"
                docker-compose -f ./pp-apt-mirror/docker-compose-sync.yml ps
                docker-compose -f ./pp-pypi-mirror/docker-compose-sync.yml ps
            else
                echo "Cannot contact http://google.com, we seem to be Offline"
            fi
            for i in "${services[@]}"
            do
                :
                docker-compose -f ./pp-$i/docker-compose.yml ps
            done
            ;;
        pull)
            for i in "${services[@]}"
                do
                    :
                    docker-compose -f ./pp-$i/docker-compose.yml pull
                done
            ;;
        save)
            # Docker
            image_folder=tmp/docker_images
            mkdir -p $image_folder
            echo "pull tag save images to $image_folder"
            list_project_images | \
            xargs -I {} -- bash -c 'echo pulling {} && docker pull {} && docker tag {} harbor.platform.net/library/{} && docker save {} > '$image_folder'/`echo {} | sed "s/\//\_/g; s/\:/\-/g"`.tar'
            ;;
        load)
            # Docker
            image_folder=tmp/docker_images
            echo "load docker images from $image_folder"
            find tmp  -regex ".*\.\(tar\)" -type f | xargs -I {} -- bash -c 'docker load -i {}'
            ;;
        push)
            if [ -z "$harbor_robot_user" ]; then
                echo 'Setup harbor robot account in harbor at http://harbor.platform.net/harbor/projects/1/robot-account'
                echo 'Run:'
                echo 'export harbor_robot_user=robot$platformplatform'
            fi
            if [ -z "$harbor_robot_user" ]; then
                echo "Setup harbor robot account key"
                echo "export harbor_robot_key='your-key'"
            fi
            # Get all images in pp-* folders
            harbor_registry="harbor.platform.net"
            harbor_project="library"
            echo "logging in to  $harbor_registry"
            docker login --username $harbor_robot_user --password $harbor_robot_key harbor.platform.net 
            echo "pull tag save images to $harbor_registry"
            list_project_images | \
            xargs -I {} -- bash -c "echo pulling {} && docker pull {} && docker tag {} $harbor_registry/$harbor_project/{} && docker push $harbor_registry/$harbor_project/{}"
            ;;
        pack)
            tar --exclude='pp-*/data' -cvf ../platformplatform.tar .
            ;;
        image-list)
            # Get all images in pp-* folders
            list_project_images
            ;;
        pump-it)
            sudo rsync -ahvP /home/platypus/Code/platformplatform /mnt/diode-downstream/platformplatform/
            ;;
        clean)
            docker system prune -a
            ;;
        kill)
            docker stop $(docker ps -a -q)
            docker rm $(docker ps -a -q)
            ;;
        nuke)
            rm .pwgen gitlab.config
            docker stop $(docker ps -a -q)
            docker rm $(docker ps -a -q)
            docker rmi $(docker images -a)
            docker volume rm $(docker volume ls |awk '{print $2}')
            docker system prune -f
            ;;
        test)
            test_curl
            ;;
        pwgen) # Generate a password file
            FILE=.pwgen

            ./pp-nginx/certs/generate.sh "./pp-nginx/certs/"
            
            if test -f "$FILE"; then
                echo "The env variable file $FILE exists, using existing passwords."
            else
                echo "No env variable file $FILE, generating new password env variables to $FILE"
                echo -n "" > $FILE
                for service in "${services[@]}"
                    do
                        :
                        count=1
                        for i in {1..5}
                            do
                                :
                                echo "export "`echo ${service^^} | sed s/\-/\_/g`"_USER_"$count=${service^^}"_USER_"$count >> $FILE
                                echo "export "`echo ${service^^} | sed s/\-/\_/g`"_PASSWORD_"$count=`pwgen -Bs1 20` >> $FILE
                                (( count++ ))
                            done
                    done
            fi

            # parse gitlab config in ./gitlab/docker-compose.yml comments into one liner and stuff in to env variable
            echo -n "`cat pp-gitlab/docker-compose.yml | grep '##' | sed 's/##\ \ \ \ //g' | grep -v '#' | sed "s/minio_aws_access_key_id/${MINIO_PASSWORD_1}/g" | sed "s/minio_aws_secret_access_key/${MINIO_PASSWORD_2}/g" |  sed ':a;N;$!ba;s/\n/\\n/g'`" > gitlab.config

            # Insert regeerated gitlab.config
            sed '/GITLAB_OMNIBUS_CONFIG_PWGEN/d' $FILE
            echo -n "export GITLAB_OMNIBUS_CONFIG_PWGEN=\"" >> $FILE
            cat gitlab.config | grep -v export |  sed ':a;N;$!ba;s/\n/\\n/g' | tr -d '\n' >>  $FILE
            echo -n "\"" >> $FILE

            # Add all variables in $FILE to env
            source $FILE

            echo ""
            echo "Credentials"
            echo "============================================="
            echo "Harbor Login     : http://harbor.platform.net"
            echo "Harbor User      : admin"
            echo "Harbor Password  : ${HARBOR_PASSWORD_5}"
            echo "MinIO Login      : http://minio.platform.net"
            echo "MinIO Access Key : ${MINIO_PASSWORD_1}"
            echo "MinIO Secret Key : ${MINIO_PASSWORD_2}"
            echo "============================================"
            echo ""

            # Not sourced warning
            if [ $sourced -eq "1" ]; then
                echo "Exported env variables from env file $FILE"
            elif [ $sourced -eq "0" ]; then
                echo "You might not have sourced this script which could mean no env variables exported, You should run '. ./platformplatform.sh pwgen' to export variables to env or delete $FILE to regenerate." #For more info how this is checked, see here - https://stackoverflow.com/a/28776166
            fi
            ;;
        *)
            echo $"Usage: $0 "
            grep -e '^\s[^\s].*)$' ./platformplatform.sh | grep -v '(' | sed s/\)//g
            exit 1
date
esac
