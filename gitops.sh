#!/bin/sh

# update interval in seconds
update_interval=10

cd "$(dirname "$0")"

if [ ! -d .git ]; then
    echo "Error: Current directory (`pwd`) is not a git repository!"
    exit 2
fi

echo "`date --iso-8601` gitops update-over-airgap"

echo -e "GET http://google.com HTTP/1.0\n\n" | nc google.com 80 > /dev/null 2>&1
if [ $? -eq 0 ]; then
    # Perform Internet side operations
    echo "GitOps running on Internet-facing server."
    
    while true
    do
        git fetch > /dev/null 2>&1
        LOCAL=$(git rev-parse HEAD)
        REMOTE=$(git rev-parse @{u})

        if [ $LOCAL != $REMOTE ]; then
            echo "New update found."
            git pull origin $(git rev-parse --abbrev-ref HEAD) > /dev/null 2>&1
            
            # save new images from compose files
            echo "Downloading new images."
            ./platformplatform.sh save > /dev/null 2>&1
            
            # zip git repo
            echo "Compressing Git repo & Docker images."
            tar --exclude='pp-*/data' --exclude='data.tar' -cvf data.tar . > /dev/null 2>&1
            
            # compress tmp folder into tarball
            #tar -cvf data.tar tmp
            
            # copy tarball to "diode"
            echo "Copying data.tar to the data diode."
            rsync -ah --progress data.tar /mnt/hgfs/DataDiode/
            rm data.tar > /dev/null 2>&1
            
            echo "Update pushed to data diode successfully."
        else
            echo "No new update found. Checking in $update_interval seconds..."
        fi
        sleep $update_interval
    done
else
    # Perform air-gap side operations
    echo "GitOps running on air-gapped server."
    update_file=/mnt/hgfs/DataDiode/data.tar
    while true
    do
        if [ -f $update_file ]; then
		    # extract tar to tmp folder
		    echo "New update found."
		    
		    echo "Updating Docker manifests and Docker images."
		    tar -C $HOME/update-over-airgap/ -xvf $update_file --keep-newer-files > /dev/null 2>&1
		    
		    # deleting update tarball
		    rm $update_file > /dev/null 2>&1
		    
		    # load new Docker images into Docker host
		    ./platformplatform.sh load > /dev/null 2>&1
		    
		    echo "Update applied! Restart the services manually by running './platformplatform.sh restart'"
	    else
	        echo "Docker containers up-to-date. Checking in $update_interval seconds..."
	    fi
	    sleep $update_interval
    done
fi

